# External sort
## Install

```bash
mkdir build; cd build; cmake -DCMAKE_INSTALL_PREFIX=../ ../; make install; cd ..
```

## Generate data

```python
python3 src/generate_data.py
```

## Sort data

```bash
./bin/external_sort data/data.txt data/res.txt
```

The proposed solution uses additional disk space to sort data. Originally, it stores intermediate data in a temporary directory (`/tmp/` in Linux by default). However, if the temporary directory does not have enough space the programm throws an exception.

To use another location for temporary data set the `TEMPDIR` environment variable to proper location. For example,
```bash
export TEMPDIR=/home/e_sha; ./bin/external_sort data/data.txt data/res.txt
```

## Check

```python
python3 src/check.py -s data/data.txt -r data/res.txt
```
