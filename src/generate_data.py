import sys
from os import makedirs
from os.path import dirname, realpath, join, isdir, basename
from random import randint, choice
from string import ascii_letters, digits
from argparse import ArgumentParser
from tqdm import tqdm
import numpy as np

def parse_args():
    parser = ArgumentParser(description='Data generator')
    parser.add_argument('-r', '--rows',
            help='number of lines to produce', type=int, default=100000)
    parser.add_argument('-l', '--length',
            help='length of strings to produce', type=int, default=100000)
    parser.add_argument('-o', '--output',
            help='name of the file to produce', default='../data/data.txt')

    return parser.parse_args(sys.argv[1:])

if __name__=='__main__':
    args = parse_args()
    assert(args.length <= 100000)

    if args.output[0] == '/':
        res_dirname = dirname(args.output)
    else:
        res_dirname = dirname(join(dirname(realpath(__file__)), args.output))
    res_filename = join(res_dirname, basename(args.output))

    if not isdir(res_dirname):
        makedirs(res_dirname)

    char_set = np.array(list(ascii_letters + digits + ',.'), dtype=np.unicode_)
    m = char_set.size
    with open(res_filename, 'w') as f:
        for _ in tqdm(range(args.rows), 'generating rows'):
            n = randint(1, args.length)
            s = ''.join(char_set[np.random.randint(0, m, size=n)])
            f.write(s+'\n')
