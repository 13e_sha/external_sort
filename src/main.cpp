#include <iostream>
#include <vector>
#include <algorithm>
#include <fstream>
#include <stdexcept>
#include <cstdio>
#include <string>
#include <numeric>
#include <functional>
#include "boost/filesystem.hpp"

using std::vector;
using std::string;
using std::endl;
using std::cerr;
using std::sort;
using std::ifstream;
using std::ofstream;
using std::runtime_error;
using std::rename;
using std::remove;
using std::to_string;
using std::getline;
using std::iota;
using std::make_heap;
using std::push_heap;
using std::pop_heap;
using std::function;
using std::min;
namespace bf=boost::filesystem;

#define err_msg(msg) (string(__FILE__) + string("[") + to_string(__LINE__) + string("]: ") + string(msg))

class IExternalSorter {
public:
  IExternalSorter(size_t batch_size = 10000):
    _batch_size(batch_size) {}

  void sort(const string &in_filename, const string &out_filename, const string &temp_dirname) {
    ifstream f(in_filename);
    if (!f.is_open())
      throw runtime_error(err_msg(string("Cannot open file ") + in_filename + string(" for reading")));

    string prefix = get_next_prefix();
    unsigned long long n = 0;
    while (true) {
      vector<string> buffer;
      string cur;
      for (size_t i = 0; i < _batch_size && getline(f, cur); ++i)
        buffer.push_back(cur);
      if (buffer.size() == 0)
        break;
      std::sort(buffer.begin(), buffer.end());

      string out = f.eof() && n == 0? out_filename: get_filename(temp_dirname, prefix, n);
      ofstream of(out);
      if (!of.is_open())
        throw runtime_error(err_msg(string("Cannot open file ") + out + string(" for writting")));
      for (auto &el: buffer) {
        of << el << endl;
        if (of.bad())
          throw runtime_error(err_msg(string("Cannot write ") +
                      to_string(el.size()) +
                      string(" symbols to ") + out));
      }
      ++n;
    }

    while (n > 1) {
      n = merge(temp_dirname, prefix, n, out_filename);
      prefix = get_next_prefix(prefix);
    }
  }
protected:
  size_t _batch_size;

  static string get_next_prefix(string prefix="") {
    if (prefix == "" || prefix == "s")
      return "f";
    return "s";
  }

  string get_filename(const string &temp_dirname, const string &prefix, unsigned long long n) {
      return temp_dirname + string("/") + prefix + to_string(n);
  }

  virtual unsigned long long merge(string temp_dirname, string prefix, unsigned long long n, string out_filename) = 0;
};

class MergeExternalSorter: public IExternalSorter {
public:
  MergeExternalSorter(size_t batch_size = 10000):
    IExternalSorter(batch_size) {}

private:

  unsigned long long merge(string temp_dirname, string prefix, unsigned long long n, string out_filename) {
    unsigned long long next_n = n / 2 + n % 2;
    string next_prefix = get_next_prefix(prefix);
    for (auto i = 0; 2 * i + 1 < n; ++i) {
      string next_filename = next_n == 1? out_filename: get_filename(temp_dirname, next_prefix, i);
      merge(get_filename(temp_dirname, prefix, 2 * i),
          get_filename(temp_dirname, prefix, 2 * i + 1),
          next_filename);
    }
    if (n % 2)
      rename(get_filename(temp_dirname, prefix, n - 1).c_str(),
          get_filename(temp_dirname, next_prefix, n / 2).c_str());
    return next_n;
  }

  void merge(const string in1, const string in2, const string out) {
    ifstream f1(in1);
    if (!f1.is_open())
      throw runtime_error(err_msg(string("Cannot open file ") + in1 + string(" for reading")));
    ifstream f2(in2);
    if (!f2.is_open())
      throw runtime_error(err_msg(string("Cannot open file ") + in2 + string(" for reading")));
    ofstream fo(out);
    if (!fo.is_open())
      throw runtime_error(err_msg(string("Cannot open file ") + out + string(" for writing")));
    string s1, s2;
    getline(f1, s1);
    getline(f2, s2);
    while (true) {
      if (s1 < s2) {
        fo << s1 << endl;
        getline(f1, s1);
        if (f1.eof())
          break;
      }
      else {
        fo << s2 << endl;
        getline(f2, s2);
        if (f2.eof())
          break;
      }
    }
    while (!f1.eof()) {
      fo << s1 << endl;
      getline(f1, s1);
    }
    while (!f2.eof()) {
      fo << s2 << endl;
      getline(f2, s2);
    }
    remove(in1.c_str());
    remove(in2.c_str());
  }
};

class HeapExternalSorter: public IExternalSorter {
public:
  HeapExternalSorter(size_t heap_size = 1000, size_t batch_size = 10000):
    _heap_size(heap_size), IExternalSorter(batch_size) {}

private:
  size_t _heap_size;

  unsigned long long merge(string temp_dirname, string prefix, unsigned long long n, string out_filename) {
    unsigned long long next_n = (n - 1) / _heap_size + 1;
    string next_prefix = get_next_prefix(prefix);
    for (auto i = 0; _heap_size * i < n; ++i) {
      size_t cur_size = min<size_t>(n - i * _heap_size, _heap_size);
      vector<ifstream> file_array(cur_size);
      vector<string> data(cur_size);
      vector<size_t> idx_array(cur_size);
      function<bool(size_t, size_t)> comp_fun = [&data](size_t i, size_t j) {return data[i] > data[j];};

      iota(idx_array.begin(), idx_array.end(), 0);
      for (auto j = 0; j < cur_size; ++j) {
        string filename = get_filename(temp_dirname, prefix, i * _heap_size + j);
        file_array[j].open(filename);
        if (!file_array[j].is_open())
          throw runtime_error(err_msg(string("Cannot open file ") + filename + string(" for reading")));
        getline(file_array[j], data[j]);
      }
      string next_filename = next_n == 1? out_filename: get_filename(temp_dirname, next_prefix, i);
      ofstream of(next_filename);
      if (!of.is_open())
        throw runtime_error(err_msg(string("Cannot open file ") + next_filename + string(" for writing")));

      make_heap(idx_array.begin(), idx_array.end(), comp_fun);
      while (idx_array.size() > 0) {
        size_t file_idx = idx_array.front();
        of << data[file_idx] << endl;
        if (of.bad())
          throw runtime_error(err_msg(string("Cannot write ") +
                      to_string(data[file_idx].size()) +
                      string(" symbols to ") + next_filename));
        pop_heap(idx_array.begin(), idx_array.end(), comp_fun); idx_array.pop_back();

        string s;
        getline(file_array[file_idx], s);
        if (!file_array[file_idx].eof()) {
          data[file_idx] = s;
          idx_array.push_back(file_idx); push_heap(idx_array.begin(), idx_array.end(), comp_fun);
        }
        else {
          file_array[file_idx].close();
          remove(get_filename(temp_dirname, prefix, i * _heap_size + file_idx).c_str());
        }
      }
    }
    return next_n;
  }
};

int main(int argc, char *argv[]) {
  if (argc != 3) {
    cerr << "Wrong number of inputs!" << endl 
      << "\t" << "Usage:\t outer_sort <in_filename> <out_filename>" << endl;
    return(-1);
  }
  bf::path temp_dirname = bf::temp_directory_path() / bf::unique_path();
  bf::create_directory(temp_dirname);
  string in_filename(argv[1]), out_filename(argv[2]);

  HeapExternalSorter s;
  s.sort(in_filename, out_filename, temp_dirname.string());

  bf::remove(temp_dirname);
}
