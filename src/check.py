import sys
import os
from argparse import ArgumentParser

def parse_args():
    parser = ArgumentParser(description='checks that the rows are sorted')
    parser.add_argument('-s', '--source', help='Source filename', required=True)
    parser.add_argument('-r', '--result', help='Result filename', required=True)

    return parser.parse_args(sys.argv[1:])

if __name__=='__main__':
    args = parse_args()

    prev = None
    is_correct = True
    # check order
    n = 160
    for line in open(args.result):
        if prev == None:
            prev = line
        else:
            is_correct &= prev <= line
            if not is_correct:
                print(prev[:n])
                print(line[:n])
                break
            prev = line
    # check size
    is_correct &= os.stat(args.source).st_size == os.stat(args.result).st_size
    if is_correct:
        print('PASSED')
    else:
        print('FAILED')
